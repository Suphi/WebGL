[Documentation](https://suphi.gitlab.io/WebGL/documentation.html)\
[Download](webgl.js)

| Examples                                                             | Source Code                                                          |
| -------------------------------------------------------------------- | -------------------------------------------------------------------- |
| [Simple](https://suphi.gitlab.io/WebGL/simple.html)                  | [simple.html](Examples/simple.html)                                  |
| [Texture](https://suphi.gitlab.io/WebGL/texture.html)                | [texture.html](Examples/texture.html)                                |
| [Lighting](https://suphi.gitlab.io/WebGL/lighting.html)              | [lighting.html](Examples/lighting.html)                              |
| [2D Sprites](https://suphi.gitlab.io/WebGL/sprite.html)              | [sprite.html](Examples/sprite.html)                                  |
| [Blocks](https://suphi.gitlab.io/WebGL/blocks.html)                  | [blocks.html](Examples/blocks.html)                                  |
| [Flight Simulator](https://suphi.gitlab.io/WebGL/flight.html)        | [flight.html](Examples/flight.html)                                  |

#### Quick Start
```javascript
//Initializes the graphics library
let gl = new GL(800, 600, 60, {"appendTo": document.body, "clearColor": [0.0, 0.0, 0.0, 1.0], "cullFace": false});
if (gl.webgl) {
	//Position camera to the front and look to the center
	gl.camera.position = [0.0, 0.0, 2.0];
	gl.camera.lookAt([0.0, 0.0, 0.0], [0.0, 1.0, 0.0]);

	//create buffers
	let buffers = gl.createBuffers({
		"indices": [0, 1, 2],
		"positions": [0.0,0.5,0.0, 0.5,-0.5,0.0, -0.5,-0.5,0.0]
	});

	//create object
	let object = new GLObject(buffers);
	object.color = [1.0, 0.0, 0.0, 1.0];
	gl.addObject(object);

	//Inside the loop rotate the object on its Y axis by 1 degrees and then draw
	window.requestAnimationFrame(loop);
	function loop(timestamp) {
		object.rotateY(1);
		gl.draw();
		window.requestAnimationFrame(loop);
	}
}
```