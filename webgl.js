class GL {
	constructor(width, height, fov, options) {
		this.canvas = document.createElement("canvas");
		this.canvas.width = width;
		this.canvas.height = height
		this.webgl = this.canvas.getContext("webgl");
		if (this.webgl == null) return;
		let vertexShader = this.webgl.createShader(this.webgl.VERTEX_SHADER);
		this.webgl.shaderSource(vertexShader, `
			attribute vec4 aPosition; attribute vec4 aNormal; attribute vec2 aCoordinate;
			uniform mat4 uProjection; uniform mat4 uModelView; uniform mat4 uNormal;
			uniform vec3 uAmbient; uniform vec3 uDirectional; uniform vec3 uDirection;
			varying vec2 vCoordinate; varying vec3 vLight;
			void main() {
				gl_Position = uProjection * uModelView * aPosition;
				vCoordinate = aCoordinate;
				if (aNormal.x != 0.0 || aNormal.y != 0.0 || aNormal.z != 0.0)
					vLight = uAmbient + (uDirectional * max(dot((uNormal * aNormal).xyz, uDirection), 0.0));
				else
					vLight = vec3(1.0, 1.0, 1.0);
			}
		`);
		this.webgl.compileShader(vertexShader);
		if (!this.webgl.getShaderParameter(vertexShader, this.webgl.COMPILE_STATUS))
			console.log("Vertex Shader:\n" + this.webgl.getShaderInfoLog(vertexShader));
		let fragmentShader = this.webgl.createShader(this.webgl.FRAGMENT_SHADER);
		this.webgl.shaderSource(fragmentShader, `
			precision highp float; uniform vec4 uColor; uniform sampler2D uSampler; varying vec2 vCoordinate; varying vec3 vLight;
			void main() {
				gl_FragColor = texture2D(uSampler, vCoordinate) * uColor;
				if(uColor.a == 1.0 && gl_FragColor.a < 0.5) discard;
				gl_FragColor.rgb *= vLight;
			}
		`);
		this.webgl.compileShader(fragmentShader);
		if (!this.webgl.getShaderParameter(fragmentShader, this.webgl.COMPILE_STATUS))
			console.log("Fragment Shader:\n" + this.webgl.getShaderInfoLog(fragmentShader));
		let program = this.webgl.createProgram();
		this.webgl.attachShader(program, vertexShader);
		this.webgl.attachShader(program, fragmentShader);
		this.webgl.linkProgram(program);
		if (!this.webgl.getProgramParameter(program, this.webgl.LINK_STATUS))
			console.log("Program:\n" + this.webgl.getProgramInfoLog(program));
		this.webgl.useProgram(program);
		this.attribute = {};
		this.attribute.position = this.webgl.getAttribLocation(program, "aPosition");
		this.attribute.normal = this.webgl.getAttribLocation(program, "aNormal");
		this.attribute.coordinate = this.webgl.getAttribLocation(program, "aCoordinate");
		this.uniform = {};
		this.uniform.projection = this.webgl.getUniformLocation(program, "uProjection");
		this.uniform.modelView = this.webgl.getUniformLocation(program, "uModelView");
		this.uniform.normal = this.webgl.getUniformLocation(program, "uNormal");
		this.uniform.ambient = this.webgl.getUniformLocation(program, "uAmbient");
		this.uniform.directional = this.webgl.getUniformLocation(program, "uDirectional");
		this.uniform.direction = this.webgl.getUniformLocation(program, "uDirection");
		this.uniform.color = this.webgl.getUniformLocation(program, "uColor");
		this.current = {};
		this.current.indiceBuffer = null;
		this.current.positionBuffer = null;
		this.current.normalBuffer = null;
		this.current.coordinateBuffer = null;
		this.current.texture = null;
		this.objects = [];
		this.camera = new GLTransform();
		this.camera.scaleZ = -1;
		this.texture = this.webgl.createTexture();
		this.webgl.bindTexture(this.webgl.TEXTURE_2D, this.texture);
		this.webgl.texImage2D(this.webgl.TEXTURE_2D, 0, this.webgl.RGBA, 1, 1, 0, this.webgl.RGBA, this.webgl.UNSIGNED_BYTE, new Uint8Array([255, 255, 255, 255]));
		this.sort = false;
		this.setFov(fov);
		this.setAmbientLight([0.8, 0.8, 0.8]);
		this.setDirectionalLight([0.5, 0.5, 0.5]);
		this.setLightDirection([0.1, -1.0, 0.2]);
		this.webgl.enable(this.webgl.CULL_FACE)
		this.webgl.enable(this.webgl.DEPTH_TEST);
		this.webgl.enable(this.webgl.BLEND);
		this.webgl.blendFuncSeparate(this.webgl.SRC_ALPHA, this.webgl.ONE_MINUS_SRC_ALPHA, this.webgl.ONE, this.webgl.ONE_MINUS_SRC_ALPHA);
		if (options) {
			if (options.appendTo) options.appendTo.appendChild(this.canvas);
			if (options.clearColor) this.setClearColor(options.clearColor);
			if (options.cullFace == false) this.webgl.disable(this.webgl.CULL_FACE);
			if (options.sort == true) this.sort = true;
		}
	}
	setSize(width, height) {
		this.webgl.canvas.width = width;
		this.webgl.canvas.height = height;
		this.depth = this.webgl.canvas.height / (2 * Math.tan(this.fov / 2));
		this.webgl.viewport(0, 0, this.webgl.canvas.width, this.webgl.canvas.height)
		let aspect = this.webgl.canvas.width/this.webgl.canvas.height;
		let near = 0.1;
		let far = this.depth*2;
		let f = 1.0 / Math.tan(this.fov / 2);
		let nf = 1 / (near - far);
		this.webgl.uniformMatrix4fv(this.uniform.projection, false,
			[f / aspect, 0, 0, 0, 0, f, 0, 0, 0, 0, (far + near) * nf, -1, 0, 0, (2 * far * near) * nf, 0]);
	}
	setFov(fov, radian) {
		if (!radian) fov *= Math.PI / 180;
		this.fov = fov;
		this.depth = this.webgl.canvas.height / (2 * Math.tan(this.fov / 2));
		let aspect = this.webgl.canvas.width/this.webgl.canvas.height;
		let near = 0.1;
		let far = this.depth*2;
		let f = 1.0 / Math.tan(this.fov / 2);
		let nf = 1 / (near - far);
		this.webgl.uniformMatrix4fv(this.uniform.projection, false,
			[f / aspect, 0, 0, 0, 0, f, 0, 0, 0, 0, (far + near) * nf, -1, 0, 0, (2 * far * near) * nf, 0]);
	}
	setClearColor(v) {
		this.webgl.clearColor(v[0], v[1], v[2], [3]);
	}
	setAmbientLight(v) {
		this.webgl.uniform3fv(this.uniform.ambient, v);
	}
	setDirectionalLight(v) {
		this.webgl.uniform3fv(this.uniform.directional, v);
	}
	setLightDirection(v) {
		this.webgl.uniform3fv(this.uniform.direction, Vec.normalize(Vec.subtract([0,0,0],v)));
	}
	createBuffers(data, dynamic) {
		let buffers = {};
		if (data.indices) buffers.indice = this.createElementBuffer(data.indices, dynamic);
		if (data.positions) buffers.position = this.createBuffer(data.positions, dynamic);
		if (data.normals) buffers.normal = this.createBuffer(data.normals, dynamic);
		if (data.coordinates) buffers.coordinate = this.createBuffer(data.coordinates, dynamic);
		return buffers;
	}
	createElementBuffer(data, dynamic) {
		let buffer = this.webgl.createBuffer();
		buffer.length = data.length;
		this.webgl.bindBuffer(this.webgl.ELEMENT_ARRAY_BUFFER, buffer);
		this.webgl.bufferData(this.webgl.ELEMENT_ARRAY_BUFFER, new Uint16Array(data), dynamic ? this.webgl.DYNAMIC_DRAW : this.webgl.STATIC_DRAW);
		this.current.indiceBuffer = buffer;
		return buffer;
	}
	setElementBuffer(buffer, data) {
		buffer.length = data.length;
		this.webgl.bindBuffer(this.webgl.ELEMENT_ARRAY_BUFFER, buffer);
		this.webgl.bufferSubData(this.webgl.ELEMENT_ARRAY_BUFFER, 0, new Uint16Array(data));
		this.current.indiceBuffer = buffer;
	}
	createBuffer(data, dynamic) {
		let buffer = this.webgl.createBuffer();
		this.webgl.bindBuffer(this.webgl.ARRAY_BUFFER, buffer);
		this.webgl.bufferData(this.webgl.ARRAY_BUFFER, new Float32Array(data), dynamic ? this.webgl.DYNAMIC_DRAW : this.webgl.STATIC_DRAW);
		return buffer;
	}
	setBuffer(buffer, data) {
		this.webgl.bindBuffer(this.webgl.ARRAY_BUFFER, buffer);
		this.webgl.bufferSubData(this.webgl.ARRAY_BUFFER, 0, new Float32Array(data));
	}
	createTexture(src, filter) {
		let texture = this.webgl.createTexture();
		this.webgl.bindTexture(this.webgl.TEXTURE_2D, texture);
		this.webgl.texImage2D(this.webgl.TEXTURE_2D, 0, this.webgl.RGBA, 1, 1, 0, this.webgl.RGBA, this.webgl.UNSIGNED_BYTE, new Uint8Array([0, 0, 0, 0]));
		this.current.texture = texture;
		let image = new Image();
		image.onload = () => {
			this.webgl.bindTexture(this.webgl.TEXTURE_2D, texture);
			this.webgl.texParameteri(this.webgl.TEXTURE_2D, this.webgl.TEXTURE_WRAP_S, this.webgl.CLAMP_TO_EDGE);
			this.webgl.texParameteri(this.webgl.TEXTURE_2D, this.webgl.TEXTURE_WRAP_T, this.webgl.CLAMP_TO_EDGE);
			if (filter == true || filter == null) {
				this.webgl.texParameteri(this.webgl.TEXTURE_2D, this.webgl.TEXTURE_MIN_FILTER, this.webgl.LINEAR);
				this.webgl.texParameteri(this.webgl.TEXTURE_2D, this.webgl.TEXTURE_MAG_FILTER, this.webgl.LINEAR);
			} else {
				this.webgl.texParameteri(this.webgl.TEXTURE_2D, this.webgl.TEXTURE_MIN_FILTER, this.webgl.NEAREST);
				this.webgl.texParameteri(this.webgl.TEXTURE_2D, this.webgl.TEXTURE_MAG_FILTER, this.webgl.NEAREST);
			}
			this.webgl.texImage2D(this.webgl.TEXTURE_2D, 0, this.webgl.RGBA, this.webgl.RGBA, this.webgl.UNSIGNED_BYTE, image);
			this.current.texture = texture;
		};
		image.src = src;
		return texture;
	}
	loadFile(src, callback) {
		var request = new XMLHttpRequest();
		request.onload = () => {
			let v = []; let vt = []; let vn = []; let f = [];
			let line = request.response.split("\n");
			for(let i = 0; i < line.length; i++){
				let data = line[i].trim().split(/\s+/);
				let command = data.shift();
				if (command == "v") v.push(data.map(Number));
				if (command == "vt") vt.push(data.map(Number));
				if (command == "vn") vn.push(data.map(Number));
				if (command == "f") f.push(data);
			}
			let storage = {}; let positions = []; let coordinates = []; let normals = []; let indices = []; let indice = 0;
			function add(id) {
				if (!storage[id]) {
					let data = id.split("/");
					positions.push(-v[data[0]-1][0], v[data[0]-1][1], v[data[0]-1][2]);
					if (data[1]) coordinates.push(vt[data[1]-1][0], 1-vt[data[1]-1][1]);
					if (data[2]) normals.push(-vn[data[2]-1][0], vn[data[2]-1][1], vn[data[2]-1][2]);
					indices.push(indice);
					storage[id] = indice;
					indice++;
				} else {
					indices.push(storage[id]);
				}
			}
			for (let i = 0; i<f.length; i++) {
				for (let j = 2; j<f[i].length; j++) {
					add(f[i][0]); add(f[i][j-1]); add(f[i][j]);
				}
			}
			let data = {};
			data.indices = indices;
			data.positions = positions;
			if (normals.length == positions.length) data.normals = normals;
			if (coordinates.length == positions.length/3*2) data.coordinates = coordinates;
			callback(data);
		};
		request.open("GET", src);
		request.send();
	}
	addObject(object) {
		if (this.objects.indexOf(object) == -1)
			this.objects.push(object);
	}
	removeObject(object) {
		var i = this.objects.indexOf(object);
		if (i != -1)
			this.objects.splice(i, 1);
	}
	draw() {
		this.webgl.clear(this.webgl.COLOR_BUFFER_BIT | this.webgl.DEPTH_BUFFER_BIT);
		if (this.sort) {
			this.objects.sort((a, b) => {
				if (a.color[3] == 1 && b.color[3] == 1) return 0;
				if (a.color[3] == 1 && b.color[3] < 1) return -1;
				if (b.color[3] == 1 && a.color[3] < 1) return 1;
				let ad = Vec.distanceSquared(this.camera._position, a._position)
				let bd = Vec.distanceSquared(this.camera._position, b._position)
				if (ad < bd) return 1;
				if (ad > bd) return -1;
				return 0;
			});
		}
		let mask = true;
		this.webgl.depthMask(mask);
		for (let i = 0; i < this.objects.length; i++) {
			if (!this.objects[i].indiceBuffer) continue;
			if (mask == true && this.objects[i].color.alpha < 1) { mask = false; this.webgl.depthMask(mask);}
			if (this.current.indiceBuffer != this.objects[i].indiceBuffer) {
				this.webgl.bindBuffer(this.webgl.ELEMENT_ARRAY_BUFFER, this.objects[i].indiceBuffer);
				this.current.indiceBuffer = this.objects[i].indiceBuffer;
			}
			if (this.current.positionBuffer != this.objects[i].positionBuffer) {
				if (this.current.positionBuffer == null) this.webgl.enableVertexAttribArray(this.attribute.position);
				if (this.objects[i].positionBuffer == null) this.webgl.disableVertexAttribArray(this.attribute.position);
				else {
					this.webgl.bindBuffer(this.webgl.ARRAY_BUFFER, this.objects[i].positionBuffer);
					this.webgl.vertexAttribPointer(this.attribute.position, 3, this.webgl.FLOAT, false, 0, 0);
				}
				this.current.positionBuffer = this.objects[i].positionBuffer;
			}
			if (this.current.normalBuffer != this.objects[i].normalBuffer) {
				if (this.current.normalBuffer == null) this.webgl.enableVertexAttribArray(this.attribute.normal);
				if (this.objects[i].normalBuffer == null) this.webgl.disableVertexAttribArray(this.attribute.normal);
				else {
					this.webgl.bindBuffer(this.webgl.ARRAY_BUFFER, this.objects[i].normalBuffer);
					this.webgl.vertexAttribPointer(this.attribute.normal, 3, this.webgl.FLOAT, false, 0, 0);
				}
				this.current.normalBuffer = this.objects[i].normalBuffer;
			}
			if (this.current.coordinateBuffer != this.objects[i].coordinateBuffer) {
				if (this.current.coordinateBuffer == null) this.webgl.enableVertexAttribArray(this.attribute.coordinate);
				if (this.objects[i].coordinateBuffer == null) this.webgl.disableVertexAttribArray(this.attribute.coordinate);
				else {
					this.webgl.bindBuffer(this.webgl.ARRAY_BUFFER, this.objects[i].coordinateBuffer);
					this.webgl.vertexAttribPointer(this.attribute.coordinate, 2, this.webgl.FLOAT, false, 0, 0);
				}
				this.current.coordinateBuffer = this.objects[i].coordinateBuffer;
			}
			if (this.current.texture != this.objects[i].texture) {
				if (this.objects[i].texture) this.webgl.bindTexture(this.webgl.TEXTURE_2D, this.objects[i].texture);
				else this.webgl.bindTexture(this.webgl.TEXTURE_2D, this.texture);
				this.current.texture = this.objects[i].texture;
			}
			let model = this.objects[i].getMatrix();
			this.webgl.uniformMatrix4fv(this.uniform.normal, false, Mat.transpose(Mat.invert(model)));
			this.webgl.uniformMatrix4fv(this.uniform.modelView, false, Mat.multiply(this.camera.getInverseMatrix(), model));
			this.webgl.uniform4fv(this.uniform.color, this.objects[i].color);
			this.webgl.drawElements(this.webgl.TRIANGLES, this.objects[i].indiceBuffer.length, this.webgl.UNSIGNED_SHORT, 0);
		}
	}
}
class GLTransform {
	get position() { return this._position; } set position(v) { this._transformed = true; this._position = v; }
	get positionX() { return this._position[0]; } set positionX(n) { this._transformed = true; this._position[0] = n; }
	get positionY() { return this._position[1]; } set positionY(n) { this._transformed = true; this._position[1] = n; }
	get positionZ() { return this._position[2]; } set positionZ(n) { this._transformed = true; this._position[2] = n; }
	get x() { return this._position[0]; } set x(n) { this._transformed = true; this._position[0] = n; }
	get y() { return this._position[1]; } set y(n) { this._transformed = true; this._position[1] = n; }
	get z() { return this._position[2]; } set z(n) { this._transformed = true; this._position[2] = n; }
	get rotation() { return this._rotation; } set rotation(v) { this._transformed = true; this._rotation = v; }
	get rotationX() { return this._rotation[0]; } set rotationX(n) { this._transformed = true; this._rotation[0] = n; }
	get rotationY() { return this._rotation[1]; } set rotationY(n) { this._transformed = true; this._rotation[1] = n; }
	get rotationZ() { return this._rotation[2]; } set rotationZ(n) { this._transformed = true; this._rotation[2] = n; }
	get rotationW() { return this._rotation[3]; } set rotationW(n) { this._transformed = true; this._rotation[3] = n; }
	get scale() { return this._scale; } set scale(v) { this._transformed = true; this._scale = v; }
	get scaleX() { return this._scale[0]; } set scaleX(n) { this._transformed = true; this._scale[0] = n; }
	get scaleY() { return this._scale[1]; } set scaleY(n) { this._transformed = true; this._scale[1] = n; }
	get scaleZ() { return this._scale[2]; } set scaleZ(n) { this._transformed = true; this._scale[2] = n; }
	constructor() {
		this._position = [0, 0, 0];
		this._rotation = [0, 0, 0, 1];
		this._scale = [1, 1, 1];
		this._matrix = [1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1];
		this._matrixType = 0;
		this._transformed = false;
	}
	move(v) {
		this._transformed = true;
		this._position = Vec.add(this._position, v);
	}
	translate(v) {
		this._transformed = true;
		this._position = Vec.translate(this._position, v, this._rotation);
	}
	eulerAngle(v, radian) {
		this._transformed = true;
		this._rotation = Quat.eulerAngle(v, radian);
	}
	axisAngle(v, n, radian) {
		this._transformed = true;
		this._rotation = Quat.axisAngle(v, n, radian);
	}
	lookAt(v1, v2) {
		this._transformed = true;
		this._rotation = Quat.lookAt(this._position, v1, v2);
	}
	rotateX(n, radian) {
		this._transformed = true;
		this._rotation = Quat.rotateX(this._rotation, n, radian);
	}
	rotateY(n, radian) {
		this._transformed = true;
		this._rotation = Quat.rotateY(this._rotation, n, radian);
	}
	rotateZ(n, radian) {
		this._transformed = true;
		this._rotation = Quat.rotateZ(this._rotation, n, radian);
	}
	getMatrix() {
		if (this._matrixType != 0) { this._matrixType = 0; this._transformed = true; }
		if (this._transformed) { this._transformed = false; this._matrix = Mat.create(this._position, this._rotation, this._scale); }
		return this._matrix;
	}
	getInverseMatrix() {
		if (this._matrixType != 1) { this._matrixType = 1; this._transformed = true; }
		if (this._transformed) { this._transformed = false; this._matrix = Mat.createInverse(this._position, this._rotation, this._scale); }
		return this._matrix;
	}
}
class GLObject extends GLTransform{
	constructor(buffers, texture) {
		super();
		if (!buffers) buffers = {};
		this.indiceBuffer = buffers.indice || null;
		this.positionBuffer = buffers.position || null;
		this.normalBuffer = buffers.normal || null;
		this.coordinateBuffer = buffers.coordinate || null;
		this.texture = texture || null;;
		this.color = [1, 1, 1, 1];
	}
	setBuffers(buffers) {
		this.indiceBuffer = buffers.indice || null;
		this.positionBuffer = buffers.position || null;
		this.normalBuffer = buffers.normal || null;
		this.coordinateBuffer = buffers.coordinate || null;
	}
}
class Vec {
	static add(v1, v2) {
		return [v1[0] + v2[0], v1[1] + v2[1], v1[2] + v2[2]];
	}
	static subtract(v1, v2) {
		return [v1[0] - v2[0], v1[1] - v2[1], v1[2] - v2[2]];
	}
	static multiply(v1, v2) {
		return [v1[0] * v2[0], v1[1] * v2[1], v1[2] * v2[2]];
	}
	static divide(v1, v2) {
		return [v1[0] / v2[0], v1[1] / v2[1], v1[2] / v2[2]];
	}
	static translate(v1, v2, q) {
		let uvx = q[1] * v2[2] - q[2] * v2[1];
		let uvy = q[2] * v2[0] - q[0] * v2[2];
		let uvz = q[0] * v2[1] - q[1] * v2[0];
		let uuvx = (q[1] * uvz - q[2] * uvy) * 2;
		let uuvy = (q[2] * uvx - q[0] * uvz) * 2;
		let uuvz = (q[0] * uvy - q[1] * uvx) * 2;
		let w2 = q[3] * 2; uvx *= w2; uvy *= w2; uvz *= w2;
		return [v1[0] + v2[0] + uvx + uuvx, v1[1] + v2[1] + uvy + uuvy, v1[2] + v2[2] + uvz + uuvz];
	}
	static normalize(v) {
		let len = v[0] * v[0] + v[1] * v[1] + v[2] * v[2];
		if (len > 0) len = 1 / Math.sqrt(len);
		return [v[0] * len, v[1] * len, v[2] * len];
	}
	static dot(v1, v2) {
		return v1[0] * v2[0] + v1[1] * v2[1] + v1[2] * v2[2];
	}
	static cross(v1, v2) {
		return [v1[1] * v2[2] - v1[2] * v2[1], v1[2] * v2[0] - v1[0] * v2[2], v1[0] * v2[1] - v1[1] * v2[0]];
	}
	static distance(v1, v2) {
		return Math.hypot(v2[0] - v1[0], v2[1] - v1[1], v2[2] - v1[2]);
	}
	static distanceSquared(v1, v2) {
		return (v2[0] - v1[0]) * (v2[0] - v1[0]) + (v2[1] - v1[1]) * (v2[1] - v1[1]) + (v2[2] - v1[2]) * (v2[2] - v1[2]);
	}
}
class Quat {
	static eulerAngle(v, radian) {
		v = Vec.multiply(v , [0.5, 0.5, 0.5]);
		if (!radian) v = Vec.multiply(v , [Math.PI / 180, Math.PI / 180, Math.PI / 180]);
		let sx = Math.sin(v[0]); let cx = Math.cos(v[0]);
		let sy = Math.sin(v[1]); let cy = Math.cos(v[1]);
		let sz = Math.sin(v[2]); let cz = Math.cos(v[2]);
		return [sx * cy * cz - cx * sy * sz, cx * sy * cz + sx * cy * sz, cx * cy * sz - sx * sy * cz, cx * cy * cz + sx * sy * sz];
	}
	static axisAngle(v, n, radian) {
		n *= 0.5; if (!radian) n *= Math.PI / 180.0;
		let s = Math.sin(n);
		return [s * v[0], s * v[1], s * v[2], Math.cos(n)]
	}
	static lookAt(v1, v2, v3) {
		let f = Vec.normalize(Vec.subtract(v2, v1));
		let r = Vec.normalize(Vec.cross(v3, f));
		let u = Vec.cross(f, r);
		let trace = r[0] + u[1] + f[2];
		if (trace > 0.0) {
			let s = 0.5 / Math.sqrt(trace + 1.0);
			return [(u[2] - f[1]) * s, (f[0] - r[2]) * s, (r[1] - u[0]) * s, 0.25 / s];
		} else {
			if (r[0] > u[1] && r[0] > f[2]) {
				let s = 2.0 * Math.sqrt(1.0 + r[0] - u[1] - f[2]);
				return [0.25 * s, (u[0] + r[1]) / s, (f[0] + r[2]) / s, (u[2] - f[1]) / s];
			} else if (u[1] > f[2]) {
				let s = 2.0 * Math.sqrt(1.0 + u[1] - r[0] - f[2]);
				return [(u[0] + r[1]) / s, 0.25 * s, (f[1] + u[2]) / s, (f[0] - r[2]) / s];
			} else {
				let s = 2.0 * Math.sqrt(1.0 + f[2] - r[0] - u[1]);
				return [(f[0] + r[2]) / s, (f[1] + u[2]) / s, 0.25 * s, (r[1] - u[0]) / s];
			}
		}
	}
	static rotateX(q, n, radian) {
		n *= 0.5; if (!radian) n *= Math.PI / 180.0;
		let s = Math.sin(n); let c = Math.cos(n);
		return [q[0] * c + q[3] * s, q[1] * c + q[2] * s, q[2] * c - q[1] * s, q[3] * c - q[0] * s];
	}
	static rotateY(q, n, radian) {
		n *= 0.5; if (!radian) n *= Math.PI / 180.0;
		let s = Math.sin(n); let c = Math.cos(n);
		return [q[0] * c - q[2] * s, q[1] * c + q[3] * s, q[2] * c + q[0] * s, q[3] * c - q[1] * s];
	}
	static rotateZ(q, n, radian) {
		n *= 0.5; if (!radian) n *= Math.PI / 180.0;
		let s = Math.sin(n); let c = Math.cos(n);
		return [q[0] * c - q[1] * s, q[1] * c + q[0] * s, q[2] * c - q[3] * s, q[3] * c + q[2] * s];
	}
	static forward(q) {
		return [2 * (q[0]*q[2] + q[3]*q[1]), 2 * (q[1]*q[2] - q[3]*q[0]), 1 - 2 * (q[0]*q[0] + q[1]*q[1])];
	}
	static right(q) {
		return [1 - 2 * (q[1]*q[1] + q[2]*q[2]), 2 * (q[0]*q[1] + q[3]*q[2]), 2 * (q[0]*q[2] - q[3]*q[1])];
	}
	static up(q) {
		return [2 * (q[0]*q[1] - q[3]*q[2]), 1 - 2 * (q[0]*q[0] + q[2]*q[2]), 2 * (q[1]*q[2] + q[3]*q[0])];
	}
}
class Mat {
	static multiply(m1, m2) {
		return [
			m2[ 0] * m1[ 0] + m2[ 1] * m1[ 4] + m2[ 2] * m1[ 8] + m2[ 3] * m1[12],
			m2[ 0] * m1[ 1] + m2[ 1] * m1[ 5] + m2[ 2] * m1[ 9] + m2[ 3] * m1[13],
			m2[ 0] * m1[ 2] + m2[ 1] * m1[ 6] + m2[ 2] * m1[10] + m2[ 3] * m1[14],
			m2[ 0] * m1[ 3] + m2[ 1] * m1[ 7] + m2[ 2] * m1[11] + m2[ 3] * m1[15],
			m2[ 4] * m1[ 0] + m2[ 5] * m1[ 4] + m2[ 6] * m1[ 8] + m2[ 7] * m1[12],
			m2[ 4] * m1[ 1] + m2[ 5] * m1[ 5] + m2[ 6] * m1[ 9] + m2[ 7] * m1[13],
			m2[ 4] * m1[ 2] + m2[ 5] * m1[ 6] + m2[ 6] * m1[10] + m2[ 7] * m1[14],
			m2[ 4] * m1[ 3] + m2[ 5] * m1[ 7] + m2[ 6] * m1[11] + m2[ 7] * m1[15],
			m2[ 8] * m1[ 0] + m2[ 9] * m1[ 4] + m2[10] * m1[ 8] + m2[11] * m1[12],
			m2[ 8] * m1[ 1] + m2[ 9] * m1[ 5] + m2[10] * m1[ 9] + m2[11] * m1[13],
			m2[ 8] * m1[ 2] + m2[ 9] * m1[ 6] + m2[10] * m1[10] + m2[11] * m1[14],
			m2[ 8] * m1[ 3] + m2[ 9] * m1[ 7] + m2[10] * m1[11] + m2[11] * m1[15],
			m2[12] * m1[ 0] + m2[13] * m1[ 4] + m2[14] * m1[ 8] + m2[15] * m1[12],
			m2[12] * m1[ 1] + m2[13] * m1[ 5] + m2[14] * m1[ 9] + m2[15] * m1[13],
			m2[12] * m1[ 2] + m2[13] * m1[ 6] + m2[14] * m1[10] + m2[15] * m1[14],
			m2[12] * m1[ 3] + m2[13] * m1[ 7] + m2[14] * m1[11] + m2[15] * m1[15]
		];
	}
	static create(v1, q, v2) {
		let xx = q[0] * q[0]; let xy = q[0] * q[1]; let xz = q[0] * q[2];
		let yy = q[1] * q[1]; let yz = q[1] * q[2]; let zz = q[2] * q[2];
		let xw = q[0] * q[3]; let yw = q[1] * q[3]; let zw = q[2] * q[3];
		return [
			(1 - 2 * (yy + zz)) * v2[0],      2 * (xy + zw)  * v2[0],      2 * (xz - yw)  * v2[0], 0,
			     2 * (xy - zw)  * v2[1], (1 - 2 * (xx + zz)) * v2[1],      2 * (yz + xw)  * v2[1], 0,
			     2 * (xz + yw)  * v2[2],      2 * (yz - xw)  * v2[2], (1 - 2 * (xx + yy)) * v2[2], 0,
			v1[0],                       v1[1],                       v1[2],                       1
		];
	}
	static createInverse(v1, q, v2) {
		let xx = q[0] * q[0]; let xy = q[0] * q[1]; let xz = q[0] * q[2];
		let yy = q[1] * q[1]; let yz = q[1] * q[2]; let zz = q[2] * q[2];
		let xw = q[0] * q[3]; let yw = q[1] * q[3]; let zw = q[2] * q[3];
		let m0 = [(1 - 2 * (yy + zz)) * v2[0],      2 * (xy + zw)  * v2[0],      2 * (xz - yw)  * v2[0]];
		let m1 = [     2 * (xy - zw)  * v2[1], (1 - 2 * (xx + zz)) * v2[1],      2 * (yz + xw)  * v2[1]];
		let m2 = [     2 * (xz + yw)  * v2[2],      2 * (yz - xw)  * v2[2], (1 - 2 * (xx + yy)) * v2[2]];
		return [
			m0[0],            m1[0],            m2[0],            0,
			m0[1],            m1[1],            m2[1],            0,
			m0[2],            m1[2],            m2[2],            0,
			-Vec.dot(m0, v1), -Vec.dot(m1, v1), -Vec.dot(m2, v1), 1
		];
	}
	static transpose(m) {
		return [m[0], m[4], m[8], m[12], m[1], m[5], m[9], m[13], m[2], m[6], m[10], m[14], m[3], m[7], m[11], m[15]];
	}
	static invert(m) {
		let m00 = m[ 0] * m[ 5] - m[ 1] * m[ 4];
		let m01 = m[ 0] * m[ 6] - m[ 2] * m[ 4];
		let m02 = m[ 0] * m[ 7] - m[ 3] * m[ 4];
		let m03 = m[ 1] * m[ 6] - m[ 2] * m[ 5];
		let m04 = m[ 1] * m[ 7] - m[ 3] * m[ 5];
		let m05 = m[ 2] * m[ 7] - m[ 3] * m[ 6];
		let m06 = m[ 8] * m[13] - m[ 9] * m[12];
		let m07 = m[ 8] * m[14] - m[10] * m[12];
		let m08 = m[ 8] * m[15] - m[11] * m[12];
		let m09 = m[ 9] * m[14] - m[10] * m[13];
		let m10 = m[ 9] * m[15] - m[11] * m[13];
		let m11 = m[10] * m[15] - m[11] * m[14];
		let det = 1 / (m00 * m11 - m01 * m10 + m02 * m09 + m03 * m08 - m04 * m07 + m05 * m06);
		return [
			(m[ 5] * m11 - m[ 6] * m10 + m[ 7] * m09) * det,
			(m[ 2] * m10 - m[ 1] * m11 - m[ 3] * m09) * det,
			(m[13] * m05 - m[14] * m04 + m[15] * m03) * det,
			(m[10] * m04 - m[ 9] * m05 - m[11] * m03) * det,
			(m[ 6] * m08 - m[ 4] * m11 - m[ 7] * m07) * det,
			(m[ 0] * m11 - m[ 2] * m08 + m[ 3] * m07) * det,
			(m[14] * m02 - m[12] * m05 - m[15] * m01) * det,
			(m[ 8] * m05 - m[10] * m02 + m[11] * m01) * det,
			(m[ 4] * m10 - m[ 5] * m08 + m[ 7] * m06) * det,
			(m[ 1] * m08 - m[ 0] * m10 - m[ 3] * m06) * det,
			(m[12] * m04 - m[13] * m02 + m[15] * m00) * det,
			(m[ 9] * m02 - m[ 8] * m04 - m[11] * m00) * det,
			(m[ 5] * m07 - m[ 4] * m09 - m[ 6] * m06) * det,
			(m[ 0] * m09 - m[ 1] * m07 + m[ 2] * m06) * det,
			(m[13] * m01 - m[12] * m03 - m[14] * m00) * det,
			(m[ 8] * m03 - m[ 9] * m01 + m[10] * m00) * det
		];
	}
}